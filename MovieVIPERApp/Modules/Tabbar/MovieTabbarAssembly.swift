//
//  MovieTabbarAssembly.swift
//  MovieVIPERApp
//
//  Created by Slava Starovoitov on 11.11.22.
//

import UIKit

class MovieTabbarAssembly {
    static func build() -> UITabBarController {
        let movieListModule = MovieListAssembly.build()
        
        let movieListNavVC = UINavigationController(rootViewController: movieListModule)
        movieListNavVC.tabBarItem = UITabBarItem(title: "Dashboard",
                                                 image: UIImage(systemName: "film.circle.fill")?.withTintColor(.yellow),
                                                 tag: 0)
        
        #warning("TODO: Set bookmark module")
        let bookMarkVC = UIViewController()
        bookMarkVC.view.backgroundColor = .orange
        
        bookMarkVC.tabBarItem = UITabBarItem(title: "Bookmarks",
                                             image: UIImage(systemName: "bookmark.square.fill")?.withTintColor(.yellow),
                                             tag: 1)
        
        
        let tabbar = MovieTabbar()
        tabbar.viewControllers = [movieListNavVC, bookMarkVC]
        
        return tabbar
    }
}
