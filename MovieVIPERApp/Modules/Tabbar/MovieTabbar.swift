//
//  MovieTabbar.swift
//  MovieVIPERApp
//
//  Created by Slava Starovoitov on 11.11.22.
//

import UIKit

class MovieTabbar: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.updateAppearance()
    }
}
