//
//  MovieListAssembly.swift
//  MovieVIPERApp
//
//  Created by Slava Starovoitov on 11.11.22.
//

import UIKit

class MovieListAssembly {
    static func build() -> UIViewController {
        let presenter = MovieListPresenter()
        let viewController = MovieListView()
        presenter.view = viewController
        viewController.output = presenter
        
        let router = MovieListRouter()
        presenter.router = router
        router.output = presenter
        
        let interactor = MovieListInteractor()
        interactor.output = presenter
        
        return viewController
    }
}
