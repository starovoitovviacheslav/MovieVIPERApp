//
//  MovieListView.swift
//  MovieVIPERApp
//
//  Created by Slava Starovoitov on 11.11.22.
//

import UIKit

class MovieListView: UIViewController {
    var output: MovieListViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewLoaded()
        
        view.backgroundColor = .purple
    }
    
}

extension MovieListView: MovieListViewInput {
    
}
