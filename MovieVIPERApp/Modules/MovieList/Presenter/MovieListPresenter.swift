//
//  MovieListPresenter.swift
//  MovieVIPERApp
//
//  Created by Slava Starovoitov on 11.11.22.
//

import Foundation

class MovieListPresenter {
    var view: MovieListViewInput?
    var interactor: MovieListInteractorInput?
    var router: MovieListRouterInput?
}

//MARK: - View Output

extension MovieListPresenter: MovieListViewOutput {
    func viewLoaded() {
        
    }
}

//MARK: - Interactor Output

extension MovieListPresenter: MovieListInteractorOutput {
    
}

//MARK: - Router Output

extension MovieListPresenter: MovieListRouterOutput {
    
}
