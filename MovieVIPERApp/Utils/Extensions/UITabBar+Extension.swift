//
//  UITabBar+Extension.swift
//  MovieVIPERApp
//
//  Created by Slava Starovoitov on 11.11.22.
//

import UIKit

extension UITabBar {
    func updateAppearance() {
        let tabBarAppearance = UITabBarAppearance()
        tabBarAppearance.backgroundColor = .darkGray
        tabBarAppearance.stackedLayoutAppearance.selected.titleTextAttributes = [.foregroundColor: UIColor.white]
        tabBarAppearance.stackedLayoutAppearance.normal.titleTextAttributes = [.foregroundColor: UIColor.black]
        tabBarAppearance.stackedLayoutAppearance.normal.iconColor = UIColor.black
        tabBarAppearance.stackedLayoutAppearance.selected.iconColor = .red
       
        barTintColor = .red
        
        standardAppearance = tabBarAppearance
        
        if #available(iOS 15, *) {
            scrollEdgeAppearance = tabBarAppearance
        }
    }
}
